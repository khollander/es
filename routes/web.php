<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Symfony\Component\Process\Process;

Route::get('/', function () {
    return response()->json([
        'response' => ['signature' => [
            'server_os' => php_uname(),
            'identifier' => env('IDENTIFIER'),
            'timestamp' => \Carbon\Carbon::now()
        ]]
    ]);
});

Route::get('/ping', function (\Illuminate\Http\Request $request) {
    return response()->json([
        'response' => 'pong',
    ]);
});

Route::get('/reboot', function () {
    $process = new Process(['reboot']);
    $process->run();
    $process->enableOutput();
    return response()->json([
        'output' => $process->getOutput(),
        'status' => $process->getStatus(),
        'exit-code' => $process->getExitCode(),
        'error-output' => $process->getErrorOutput()
    ]);
});

Route::get('/shutdown', function () {
    $process = new Process(['shutdown']);
    $process->run();
    $process->enableOutput();
    return response()->json(
        ['response' =>
            [
                'output' => $process->getOutput(),
                'status' => $process->getStatus(),
                'exit-code' => $process->getExitCode(),
                'error-output' => $process->getErrorOutput()
            ]
        ]);
});

Route::get('/sysload', function () {
    return response()->json(['response' => sys_getloadavg()[0]]);
});

Route::get('/getfile/{file}', function ($file) {
    $allowed_files = [
        'yum.log'
    ];
    if ($allowed_files[0] !== '*' && !in_array($file, $allowed_files)) {
        return response()->json([
            'response' => 'File not allowed'
        ]);
    } else {
        return response()->download('/var/log/' . $file);
    }
});

Route::get('/customCommand', function ($command, $arguments) {
    if (env('CUSTOM_COMMANDS') == TRUE) {
        $process = new Process([$command, $arguments]);
        $process->run();
        $process->enableOutput();
        return response()->json(
            ['response' =>
                [
                    'output' => $process->getOutput(),
                    'status' => $process->getStatus(),
                    'exit-code' => $process->getExitCode(),
                    'error-output' => $process->getErrorOutput()
                ]
            ]);
    } else {
        return response()->json(['response' => 'Custom commands are not allowed']);
    }
});
