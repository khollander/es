<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initial installation of the slave';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $env_read = file_get_contents('.env');
        if (strpos($env_read, 'ESAPI_INSTALLED=TRUE') !== false) {
            return $this->error("This slave is already installed. Aborting...");
        }

        $this->info("Welcome! This is the very first release of ES:API. This command will help you to configure your slave.");
        $identifier = $this->ask("Please set an public identifier for this server");

        \Artisan::call("key:generate");

        $env_append = fopen(".env", "a");
        $auth_key = sha1(env('APP_KEY') . \Carbon\Carbon::now());
        fwrite($env_append, "\n\n");
        fwrite($env_append, "IDENTIFIER=" . "\"" . $identifier . "\"");
        fwrite($env_append, "\n");
        fwrite($env_append, "AUTH_KEY=" . $auth_key);
        fwrite($env_append, "\n");

        if ($this->confirm("Do you want to enable the custom commands?", true)) {
            fwrite($env_append, "CUSTOM_COMMANDS=TRUE");
            fwrite($env_append, "\n");
        } else {
            fwrite($env_append, "CUSTOM_COMMANDS=FALSE");
            fwrite($env_append, "\n");
        }

        fwrite($env_append, 'ESAPI_INSTALLED=TRUE');

        fclose($env_append);

        $this->info("Auth key:" . $auth_key);
        $this->info("Please store your auth key!");
    }
}
