<?php

namespace App\Http\Middleware;

use Closure;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header('Authorization');
        if ($header !== env('AUTH_KEY')) {
            return response()->json(['response' => 'Unauthorized'], 401);
        }
        return $next($request);
    }
}
